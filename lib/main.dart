import 'dart:js';

import 'package:flutter/material.dart';
import 'package:projeto01/pages/catalogo.dart';
import 'package:projeto01/pages/mangaList.dart';
import 'package:projeto01/pages/pages_settings/sobre.dart';
import 'pages/login.dart';
import 'pages/home.dart';
import 'pages/settings.dart';
import 'pages/catalogo.dart';
import 'pages/perfil.dart';
import 'pages/favoritos.dart';
import 'pages/cadastro.dart';

void main() {
  runApp(MaterialApp(
    debugShowCheckedModeBanner: false,
    title: "You Mangá",
    home: new TelaPrincipal(),

    //Tema
    theme: ThemeData(
      primaryColor: Colors.white,
      secondaryHeaderColor: Colors.brown.shade900,
      backgroundColor: Color.fromRGBO(64, 61, 138, 1),
      fontFamily: 'Roboto',
      textTheme: TextTheme(
          headline1: TextStyle(
        fontSize: 24,
        color: Colors.white,
      )),
    ),

    //ROTAS DE NAVEGAÇÃO
    initialRoute: 'telaLogin',
    routes: {
      '/telaLogin': (context) => TelaPrincipal(),
      '/homepage': (context) => Homepage(),
      '/catalogo': (context) => ListCatalago(),
      '/perfil': (context) => PerfilForm(),
      '/favoritos': (context) => ListFavoritos(),
      '/settings': (context) => Settings(),
      '/cadastro': (context) => TelaCadastro(),
      '/about'   : (context) => AboutPage(),
      '/mangalist':(context) => MangaList(),
      '/cadastroList':(context)=> MangaListCadastro(),
    },
  ));
}
