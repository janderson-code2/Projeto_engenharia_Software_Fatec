import 'package:cloud_firestore/cloud_firestore.dart';
import 'package:firebase_auth/firebase_auth.dart';
import 'package:flutter/material.dart';

// Define  um widget Form customizado
class TelaCadastro extends StatefulWidget {
  @override
  TelaCadastroState createState() {
    return TelaCadastroState();
  }
}

// Define a classe State que vai tratar os dados do Form
class TelaCadastroState extends State<TelaCadastro> {
  final _formKey = GlobalKey<FormState>();
  var txtLogin = TextEditingController();
  var txtSenha = TextEditingController();
  var txtEmail = TextEditingController();
  @override
  Widget build(BuildContext context) {
    return Scaffold(
      appBar: AppBar(
        title: Text(
          'Tela de Cadastro',
          style: TextStyle(color: Theme.of(context).primaryColor),
        ),
        centerTitle: true,
        backgroundColor: Theme.of(context).secondaryHeaderColor,
        leading: Builder(
          builder: (BuildContext context) {
            return IconButton(
                onPressed: () {
                  Navigator.pop(context);
                },
                icon: Icon(Icons.arrow_back),
                color: Colors.white);
          },
        ),
      ),
      backgroundColor: Theme.of(context).backgroundColor,
      body: Center(
        child: Container(
            padding: EdgeInsets.all(40),
            child: Form(
              key: _formKey,
              child: Column(
                crossAxisAlignment: CrossAxisAlignment.start,
                mainAxisAlignment: MainAxisAlignment.spaceBetween,
                children: <Widget>[
                  //Campo  Usuario
                  TextFormField(
                    controller: txtLogin,
                    style: TextStyle(color: Colors.white),
                    decoration: InputDecoration(
                        border: OutlineInputBorder(
                            borderSide: BorderSide(color: Colors.white)),
                        prefixIcon: Icon(Icons.person, color: Colors.black),
                        fillColor: Colors.white,
                        filled: true,
                        labelText: 'Usuário',
                        labelStyle:
                            TextStyle(color: Colors.black, fontSize: 16),
                        hintText: 'Digite o novo nome de Usuário',
                        hintStyle:
                            TextStyle(fontSize: 10, color: Colors.white)),
                    maxLength: 20,
                    keyboardType: TextInputType.name,
                    // The validator receives the text that the user has entered.
                    validator: (value) {
                      if (value == null || value.isEmpty) {
                        return showAlertDialog1(context);
                      }
                      return null;
                    },
                  ),

                  //Campo  Email
                  TextFormField(
                    controller: txtEmail,
                    style: TextStyle(color: Colors.white),
                    decoration: InputDecoration(
                        border: OutlineInputBorder(
                            borderSide: BorderSide(color: Colors.white)),
                        prefixIcon: Icon(Icons.email, color: Colors.black),
                        fillColor: Colors.white,
                        filled: true,
                        labelText: 'Email',
                        labelStyle:
                            TextStyle(color: Colors.black, fontSize: 16),
                        hintText: 'Digite o email',
                        hintStyle:
                            TextStyle(fontSize: 10, color: Colors.white)),
                    keyboardType: TextInputType.emailAddress,
                    // The validator receives the text that the user has entered.
                    validator: (value) {
                      if (value == null || value.isEmpty) {
                        return showAlertDialog1(context);
                      }
                      return null;
                    },
                  ),
                  //Campo senha
                  TextFormField(
                    obscureText: true,
                    controller: txtSenha,
                    style: TextStyle(color: Colors.white),
                    decoration: InputDecoration(
                        border: OutlineInputBorder(
                            borderSide: BorderSide(color: Colors.white)),
                        prefixIcon: Icon(Icons.vpn_key, color: Colors.black),
                        fillColor: Colors.white,
                        filled: true,
                        labelText: 'Senha',
                        labelStyle:
                            TextStyle(color: Colors.black, fontSize: 16),
                        hintText: 'Digite a nova senha',
                        hintStyle:
                            TextStyle(fontSize: 10, color: Colors.black)),
                    maxLength: 8,
                    // The validator receives the text that the user has entered.
                    validator: (value) {
                      if (value == null || value.isEmpty) {
                        return showAlertDialog1(context);
                      }
                      return null;
                    },
                  ),

                  //Botão para salvar dados
                  Padding(
                    padding: const EdgeInsets.symmetric(vertical: 16.0),
                    child: ElevatedButton(
                      style: ButtonStyle(
                        backgroundColor: MaterialStateProperty.all<Color>(
                            Theme.of(context).secondaryHeaderColor),
                      ),
                      onPressed: () {
                        // Validate returns true if the form is valid, or false otherwise.
                        if (_formKey.currentState!.validate()) {
                          // If the form is valid, display a snackbar. In the real world,
                          // you'd often call a server or save the information in a database.
                          criarConta(
                              txtLogin.text, txtEmail.text, txtSenha.text);
                          ScaffoldMessenger.of(context).showSnackBar(
                              SnackBar(content: Text('Salvando alterações')));
                        }
                      },
                      child: Text('Salvar'),
                    ),
                  ),
                ],
              ),
            )),
      ),
    );
    // Cria o widget Form usando  _formKey
  }

  //
  // CRIAR CONTA no Firebase Auth
  //
  void criarConta(nome, email, senha) {
    FirebaseAuth fa = FirebaseAuth.instance;
    fa
        .createUserWithEmailAndPassword(email: email, password: senha)
        .then((resultado) {
      //armazenar dados adicionais no Firestore
      var db = FirebaseFirestore.instance;
      db
          .collection('usuarios')
          .doc(resultado.user!.uid)
          .set({'nome': nome, 'email': email}).then((valor) {
        ScaffoldMessenger.of(context).showSnackBar(SnackBar(
            content: Text('Usuário criado com sucesso.'),
            duration: Duration(seconds: 2)));
        Navigator.pop(context);
      });
    }).catchError((erro) {
      if (erro.code == 'email-already-in-use') {
        ScaffoldMessenger.of(context).showSnackBar(SnackBar(
            content: Text('ERRO: O email informado já está em uso.'),
            duration: Duration(seconds: 2)));
      } else {
        ScaffoldMessenger.of(context).showSnackBar(SnackBar(
            content: Text('ERRO: ${erro.message}'),
            duration: Duration(seconds: 2)));
      }
    });
  }
}

//   Caixa de alerta
showAlertDialog1(BuildContext context) {
  // configura o button
  Widget okButton = TextButton(
    child: Text("OK"),
    onPressed: () {
      Navigator.of(context).pop();
    },
  );
  // configura o  AlertDialog
  AlertDialog alerta = AlertDialog(
    title: Text("Aviso"),
    content: Text("Preencha o campo de forma correta e completa"),
    actions: [
      okButton,
    ],
  );
  // exibe o dialog
  showDialog(
    context: context,
    builder: (BuildContext context) {
      return alerta;
    },
  );
}
