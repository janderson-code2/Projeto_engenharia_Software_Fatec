/*Modelo de dados que sera usado para encapsular os dados recuperados do Cloud Firestore */

class Manga {
  //Atributos = campos do documento

  late String id, nome, preco,autor,genero;

  //Construtor
  Manga(this.id, this.nome, this.preco,this.autor,this.genero);

  //Converter Documento em um Objeto
  Manga.fromJson(Map<String, dynamic> mapa, String id) {
    this.id = id;
    this.nome = mapa['nome'];
    this.preco = mapa['preco'];
    this.autor = mapa['autor'];
    this.genero = mapa['genero'];
  }

//Converte um objeto em um documento
  Map<String, dynamic> toJson() {
    return {'id': this.id, 'nome': this.nome, 'preco': this.preco,'genero': this.genero,'autor':this.autor};
  }
}
