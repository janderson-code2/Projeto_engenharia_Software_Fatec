import 'package:flutter/material.dart';

class ListCatalago extends StatefulWidget {
  @override
  _ListCatalagoState createState() => _ListCatalagoState();
}

class _ListCatalagoState extends State<ListCatalago> {
  @override
  Widget build(BuildContext context) {
    return Scaffold(
      appBar: AppBar(
        title: Text('Catálogo',
            style: TextStyle(color: Theme.of(context).primaryColor)),
        centerTitle: true,
        backgroundColor: Theme.of(context).secondaryHeaderColor,
        leading: Builder(
          builder: (BuildContext context) {
            return IconButton(
                onPressed: () {
                  Navigator.pop(context);
                },
                icon: Icon(Icons.arrow_back),
                color: Colors.white);
          },
        ),
      ),
      body: Container(
          color: Theme.of(context).backgroundColor,
          child: ListView(
            scrollDirection: Axis.vertical,
            padding: EdgeInsets.all(5),
            children: [
              //Container imagem
              Container(
                height: MediaQuery.of(context).size.height * 1,
                child: Column(
                  mainAxisAlignment: MainAxisAlignment.spaceBetween,
                  children: [
                    Center(
                      child: Stack(
                        alignment: AlignmentDirectional.topCenter,
                        children: <Widget>[
                          Image.asset('lib/imagens/alita01.jpg',
                              width: 500, height: 500),
                          Positioned(
                            bottom: 0,
                            child: ElevatedButton.icon(
                              onPressed: () {
                                showAlertDialog1(context);
                              },
                              icon: Icon(Icons.add),
                              label: Text(" Adicionar"),
                            ),
                          ),
                        ],
                      ),
                    ),
                  ],
                ),
              ),

              //Container imagem
              Container(
                height: MediaQuery.of(context).size.height * 1,
                child: Column(
                  mainAxisAlignment: MainAxisAlignment.spaceBetween,
                  children: [
                    Center(
                      child: Stack(
                        alignment: AlignmentDirectional.topCenter,
                        children: <Widget>[
                          Image.asset('lib/imagens/alita02.jpg',
                              width: 500, height: 500),
                          Positioned(
                            bottom: 0,
                            child: ElevatedButton.icon(
                              onPressed: () {
                                showAlertDialog1(context);
                              },
                              icon: Icon(Icons.add),
                              label: Text(" Adicionar"),
                            ),
                          ),
                        ],
                      ),
                    ),
                  ],
                ),
              ),
              Container(
                height: MediaQuery.of(context).size.height * 1,
                child: Column(
                  mainAxisAlignment: MainAxisAlignment.spaceBetween,
                  children: [
                    Center(
                      child: Stack(
                        alignment: AlignmentDirectional.topCenter,
                        children: <Widget>[
                          Image.asset('lib/imagens/alita03.jpg',
                              width: 500, height: 500),
                          Positioned(
                            bottom: 0,
                            child: ElevatedButton.icon(
                              onPressed: () {
                                showAlertDialog1(context);
                              },
                              icon: Icon(Icons.add),
                              label: Text(" Adicionar"),
                            ),
                          ),
                        ],
                      ),
                    ),
                  ],
                ),
              ),
            ],
          )),
    );
  }
}

// Caixa de dialogo usada no on pressed do ElevatedButton de cada imagem
showAlertDialog1(BuildContext context) {
  // configura o button
  Widget okButton = TextButton(
    child: Text("OK"),
    onPressed: () {
      Navigator.of(context).pop();
    },
  );
  // configura o  AlertDialog
  AlertDialog alerta = AlertDialog(
    title: Text(
      "Aviso!",
      style: TextStyle(color: Colors.red),
    ),
    content: Text("Mangá adicionado aos Favoritos com sucesso"),
    actions: [
      okButton,
    ],
  );
  // exibe o dialog
  showDialog(
    context: context,
    builder: (BuildContext context) {
      return alerta;
    },
  );
}
