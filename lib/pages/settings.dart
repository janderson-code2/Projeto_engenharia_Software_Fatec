import 'package:flutter/material.dart';

// LISTVIEW.SEPARETED
//

class Settings extends StatelessWidget {
  @override
  Widget build(BuildContext context) {
    return Scaffold(
        appBar: AppBar(
          title: Text(
            'Configurações',
            style: TextStyle(color: Theme.of(context).primaryColor),
          ),
          centerTitle: true,
          backgroundColor: Theme.of(context).secondaryHeaderColor,
          leading: Builder(
            builder: (BuildContext context) {
              return IconButton(
                  onPressed: () {
                    Navigator.pop(context);
                  },
                  icon: Icon(Icons.arrow_back),
                  color: Colors.white);
            },
          ),
        ),
        body: Container(
          color: Theme.of(context).backgroundColor,
          height: MediaQuery.of(context).size.height * 1,
          child: ListView(
            shrinkWrap: true,
            padding: EdgeInsets.all(40),
            children: <Widget>[
              ListTile(
                leading: Icon(
                  Icons.settings,
                  color: Colors.white,
                ),
                title: Text(
                  'Configurações Gerais',
                  style: TextStyle(color: Colors.white),
                ),
                trailing: Icon(Icons.arrow_forward, color: Colors.white),
              ),
              ListTile(
                leading: Icon(
                  Icons.notifications,
                  color: Colors.white,
                ),
                title: Text(
                  'Notificações',
                  style: TextStyle(color: Colors.white),
                ),
                trailing: Icon(Icons.arrow_forward, color: Colors.white),
              ),
              ListTile(
                leading: Icon(
                  Icons.privacy_tip,
                  color: Colors.white,
                ),
                title: Text(
                  'Privacidade',
                  style: TextStyle(color: Colors.white),
                ),
                trailing: Icon(Icons.arrow_forward, color: Colors.white),
              ),
              ListTile(
                leading: Icon(
                  Icons.question_answer_rounded,
                  color: Colors.white,
                ),
                title: Text(
                  'Sobre',
                  style: TextStyle(color: Colors.white),
                ),
                trailing: Icon(Icons.arrow_forward, color: Colors.white),
                onTap: () {
                  Navigator.of(context).pushNamed('/about');
                },
              )
            ],
          ),
        ));
  }
}
