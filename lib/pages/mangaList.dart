import 'package:cloud_firestore/cloud_firestore.dart';
import 'package:firebase_core/firebase_core.dart';
import 'package:flutter/material.dart';

import 'manga.dart';

Future<void> main() async {
  //Inicializar o FIRESTORE
  WidgetsFlutterBinding.ensureInitialized();
  await Firebase.initializeApp();

  //
  // TESTE DO FIRESTORE
  //
  /*
  var db = FirebaseFirestore.instance;

  //Adicionar um novo DOCUMENTO
  db.collection('mangas').add(
    {
      'nome' : 'Café Fatec Ribeirão 1 Kg',
      'preco': '14,68',
    }
  );
  */
}

//
// TELA PRINCIPAL
//
class MangaList extends StatefulWidget {
  @override
  _MangaListState createState() => _MangaListState();
}

class _MangaListState extends State<MangaList> {
  //Referenciar a coleção nomeada "mangas"
  late CollectionReference mangas;

  @override
  void initState() {
    super.initState();
    mangas = FirebaseFirestore.instance.collection("manga");
  }

  //
  // Definir a aparência de como cada documento deve ser exibido
  //
  Widget exibirDocumento(item) {
    //Converter um DOCUMENTO em um OBJETO
    Manga manga = Manga.fromJson(item.data(), item.id);

    return ListTile(
        title: Text(manga.nome,
            style: TextStyle(fontSize: 26, color: Colors.white)),
        subtitle: Text('R\$ ${manga.preco}',
            style: TextStyle(
                fontSize: 22,
                fontStyle: FontStyle.italic,
                color: Colors.white)),
        trailing: IconButton(
          icon: Icon(Icons.delete),
          onPressed: () {
            //
            // Apagar um documento da coleção "mangas"
            //
            mangas.doc(manga.id).delete();
          },
        ),
        onTap: () {
          Navigator.pushNamed(context, '/cadastroList', arguments: manga.id);
        });
  }

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      appBar: AppBar(
        title: Text('Manga List',
            style: TextStyle(color: Theme.of(context).primaryColor)),
        centerTitle: true,
        backgroundColor: Theme.of(context).secondaryHeaderColor,
        leading: Builder(
          builder: (BuildContext context) {
            return IconButton(
                onPressed: () {
                  Navigator.pop(context);
                },
                icon: Icon(Icons.arrow_back),
                color: Colors.white);
          },
        ),
      ),
      backgroundColor: Theme.of(context).backgroundColor,

      //
      // Exibir os documentos
      //
      body: Container(
        padding: EdgeInsets.all(30),
        child: StreamBuilder<QuerySnapshot>(

            //fonte de dados
            stream: mangas.snapshots(),

            //aparência
            builder: (context, snapshot) {
              switch (snapshot.connectionState) {
                case ConnectionState.none:
                  return Center(child: Text('Erro ao conectar ao Firestore'));

                case ConnectionState.waiting:
                  return Center(child: CircularProgressIndicator());

                default:
                  //dados recebidos do Firestore
                  final dados = snapshot.requireData;

                  return ListView.builder(
                      itemCount: dados.size,
                      itemBuilder: (context, index) {
                        return exibirDocumento(dados.docs[index]);
                      });
              }
            }),
      ),

      floatingActionButton: FloatingActionButton(
          foregroundColor: Colors.white,
          backgroundColor: Colors.brown,
          child: Icon(Icons.add),
          onPressed: () {
            Navigator.pushNamed(context, '/cadastroList');
          }),
    );
  }
}

//
// TELA CADASTRO
//
class MangaListCadastro extends StatefulWidget {
  @override
  _MangaListCadastroState createState() => _MangaListCadastroState();
}

class _MangaListCadastroState extends State<MangaListCadastro> {
  var txtNome = TextEditingController();
  var txtPreco = TextEditingController();
  var txtAutor = TextEditingController();
  var txtGenero = TextEditingController();

  void getDocumentById(id) async {
    await FirebaseFirestore.instance
        .collection('manga')
        .doc(id)
        .get()
        .then((resultado) {
      txtNome.text = resultado.get('nome');
      txtPreco.text = resultado.get('preco');
      txtAutor.text = resultado.get('autor');
      txtGenero.text = resultado.get('genero');
    });
  }

  @override
  Widget build(BuildContext context) {
    var id = ModalRoute.of(context)?.settings.arguments;

    if (id != null) {
      if (txtNome.text == '' &&
          txtPreco.text == '' &&
          txtAutor.text == '' &&
          txtGenero.text == '') {
        getDocumentById(id);
      }
    }

    return Scaffold(
      appBar: AppBar(
        title: Text('Mangá list'),
        centerTitle: true,
        backgroundColor: Theme.of(context).secondaryHeaderColor,
        automaticallyImplyLeading: false,
      ),
      backgroundColor: Theme.of(context).backgroundColor,
      body: Container(
        padding: EdgeInsets.all(30),
        child: ListView(children: [
          TextField(
            controller: txtNome,
            style: TextStyle(color: Colors.white, fontSize: 36),
            decoration: InputDecoration(
              labelText: 'Nome',
              labelStyle: TextStyle(color: Colors.white, fontSize: 22),
            ),
          ),
          SizedBox(height: 30),
          TextField(
            controller: txtAutor,
            style: TextStyle(color: Colors.white, fontSize: 36),
            decoration: InputDecoration(
              labelText: 'Autor',
              labelStyle: TextStyle(color: Colors.white, fontSize: 22),
            ),
          ),
          SizedBox(height: 50),
          TextField(
            controller: txtGenero,
            style: TextStyle(color: Colors.white, fontSize: 36),
            decoration: InputDecoration(
              labelText: 'Genero',
              labelStyle: TextStyle(color: Colors.white, fontSize: 22),
            ),
          ),
          SizedBox(height: 50),
          TextField(
            controller: txtPreco,
            style: TextStyle(color: Colors.white, fontSize: 36),
            decoration: InputDecoration(
              labelText: 'Preço',
              labelStyle: TextStyle(color: Colors.white, fontSize: 22),
            ),
          ),
          SizedBox(height: 50),
          Row(
            mainAxisAlignment: MainAxisAlignment.center,
            children: [
              Container(
                color: Theme.of(context).secondaryHeaderColor,
                width: 150,
                child: OutlinedButton(
                  child: Text('salvar', style: TextStyle(color: Colors.white)),
                  onPressed: () {
                    var db = FirebaseFirestore.instance;

                    if (id == null) {
                      //Adicionar um novo DOCUMENTO
                      db.collection('manga').add({
                        'nome': txtNome.text,
                        'preco': txtPreco.text,
                        'autor': txtAutor.text,
                        'genero': txtGenero.text,
                      });
                    } else {
                      db.collection('manga').doc(id.toString()).update({
                        'nome': txtNome.text,
                        'preco': txtPreco.text,
                        'autor': txtAutor.text,
                        'genero': txtGenero.text,
                      });
                    }
                    Navigator.pop(context);
                  },
                ),
              ),
              Container(
                color: Theme.of(context).secondaryHeaderColor,
                width: 150,
                child: OutlinedButton(
                  child:
                      Text('cancelar', style: TextStyle(color: Colors.white)),
                  onPressed: () {
                    Navigator.pop(context);
                  },
                ),
              ),
            ],
          )
        ]),
      ),
    );
  }
}
