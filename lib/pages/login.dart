//
// TELA PRINCIPAL
//

import 'package:firebase_auth/firebase_auth.dart';
import 'package:flutter/material.dart';

class TelaPrincipal extends StatefulWidget {
  @override
  _TelaPrincipalState createState() => _TelaPrincipalState();
}

class _TelaPrincipalState extends State<TelaPrincipal> {
  /* atributos */

  //Armazenar valores digitados no campo peso e altura

  var txtEmail = TextEditingController();
  var txtSenha = TextEditingController();
  bool isLoading = false;

  //chave que identifica unicamente o formulario

  var formKey = GlobalKey<FormState>();

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      backgroundColor: Theme.of(context).backgroundColor,
      body: Center(
        child: Container(
          padding: EdgeInsets.all(50),
          child: Form(
            key: formKey,
            child: Column(
              mainAxisAlignment: MainAxisAlignment.spaceBetween,
              children: [
                Image.asset('lib/imagens/logo.png'),
                Text(
                  "YOU MANGÁ",
                  style: TextStyle(color: Theme.of(context).primaryColor),
                ),
                campoTexto('Email', txtEmail),
                campoTextoSenha('Senha', txtSenha),
                botao('Entrar'),
                TextButton(
                  child: Text(
                    "Criar uma nova conta",
                    style: TextStyle(
                        color: Colors.white,
                        backgroundColor: Colors.transparent),
                  ),
                  onPressed: () {
                    Navigator.of(context).pushNamed('/cadastro');
                  },

                  //style: TextStyle(color: Theme.of(context).primaryColor),
                )
              ],
            ),
          ),
        ),
      ),
    );
  }
  //
  //CAMPO DE TEXTO para entrada de dados
  //

  Widget campoTexto(rotulo, variavelControle) {
    return Container(
        padding: EdgeInsets.symmetric(vertical: 5),
        child: TextFormField(
          //Associar a variável ao campo de texto
          controller: variavelControle,

          // formatação estilo da entrada
          style: TextStyle(color: Colors.black, fontSize: 22),

          keyboardType: TextInputType.number,

          decoration: InputDecoration(
            border:
                OutlineInputBorder(borderSide: BorderSide(color: Colors.white)),
            prefixIcon: Icon(Icons.person, color: Colors.black),
            fillColor: Colors.white,
            filled: true,
            labelText: rotulo,
            labelStyle: TextStyle(color: Colors.black, fontSize: 16),
            hintText: 'Digite o Email',
            hintStyle: TextStyle(fontSize: 10, color: Colors.black),
          ),
        ));
  }

  Widget campoTextoSenha(rotulo, variavelControle) {
    return Container(
        padding: EdgeInsets.symmetric(vertical: 5),
        child: TextFormField(
          //Associar a variável ao campo de texto
          controller: variavelControle,

          // formatação estilo
          obscureText: true,
          style: TextStyle(fontSize: 24, color: Colors.black),
          keyboardType: TextInputType.number,
          decoration: InputDecoration(
            border:
                OutlineInputBorder(borderSide: BorderSide(color: Colors.white)),
            prefixIcon: Icon(Icons.vpn_key, color: Colors.black),
            fillColor: Colors.white,
            filled: true,
            labelText: rotulo,
            labelStyle: TextStyle(color: Colors.black, fontSize: 16),
            hintText: 'Digite a senha',
            hintStyle: TextStyle(fontSize: 10, color: Colors.black),
          ),
        ));
  }

  //
  // BOTÃO
  //
  Widget botao(rotulo) {
    return Container(
      /*Dimensões */
      padding: EdgeInsets.only(top: 40),
      width: 200,
      height: 80,

      /*Tipo Widget  ElevatedButton*/
      child: ElevatedButton(
          style: ButtonStyle(
            backgroundColor: MaterialStateProperty.all<Color>(
                Theme.of(context).secondaryHeaderColor),
          ),
          child: Text(
            rotulo,
            style: TextStyle(color: Colors.white, fontSize: 18),
          ),
          onPressed: () {
            // Navigator.of(context).pushNamed('/homepage');
            setState(() {
              isLoading = true;
            });
            login(txtEmail.text, txtSenha.text);
            //print('botão pressionado!');
          }),
    );
  }

  //
  // LOGIN com Firebase Auth
  //
  void login(email, senha) {
    FirebaseAuth.instance
        .signInWithEmailAndPassword(email: email, password: senha)
        .then((resultado) {
      isLoading = false;
      Navigator.of(context).pushNamed('/homepage');
    }).catchError((erro) {
      var mensagem = '';
      if (erro.code == 'user-not-found') {
        mensagem = 'ERRO: Usuário não encontrado';
      } else if (erro.code == 'wrong-password') {
        mensagem = 'ERRO: Senha incorreta';
      } else if (erro.code == 'invalid-email') {
        mensagem = 'ERRO: Email inválido';
      } else {
        mensagem = erro.message;
      }

      ScaffoldMessenger.of(context).showSnackBar(
          SnackBar(content: Text('$mensagem'), duration: Duration(seconds: 2)));
    });
  }
}
