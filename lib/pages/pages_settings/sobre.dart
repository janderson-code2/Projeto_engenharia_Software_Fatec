import 'package:flutter/material.dart';
import 'package:projeto01/pages/settings.dart';

class AboutPage extends StatelessWidget {
  @override
  Widget build(BuildContext context) {
    // TODO: implement build
    return MaterialApp(
      debugShowCheckedModeBanner: false,
      home: Scaffold(
        appBar: AppBar(
          title: Center(child: Text("Sobre")),
          backgroundColor: Theme.of(context).secondaryHeaderColor,
          leading: Builder(
            builder: (BuildContext context) {
              return IconButton(
                  onPressed: () {
                   
                  },
                  icon: Icon(Icons.arrow_back),
                  color: Colors.white);
            },
          ),
        ),
        body: SingleChildScrollView(
          child: Container(
            color: Theme.of(context).backgroundColor,
            height: MediaQuery.of(context).size.height * 1,
            padding: EdgeInsets.all(40),
            child: Column(
              children: [
                SizedBox(height: 20),
                SizedBox(
                  child: Text(
                    'Você já comprou volumes de mangás duplicados? Ainda usa planilhas para guardar informações ? YouMangá está aqui para ajudar a organizar e mudar o modo como você organizar sua coleção de mangá de forma descomplicada.',
                    textAlign: TextAlign.center,
                    style: TextStyle(fontSize: 18, color: Colors.white),
                  ),
                ),
                SizedBox(height: 20),
                SizedBox(
                  child: Text(
                    'DESENVOLVEDOR: JANDERSON BARBOSA GONÇALVES',
                    textAlign: TextAlign.justify,
                    style: TextStyle(fontSize: 16, color: Colors.white),
                  ),
                ),
                SizedBox(height: 50),
                ClipRRect(
                  borderRadius: BorderRadius.circular(10),
                  child: Image.asset('lib/imagens/logo.png'),
                ),
              ],
            ),
          ),
        ),
      ),
    );
  }
}
