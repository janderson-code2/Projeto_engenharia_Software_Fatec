import 'package:flutter/material.dart';


class Homepage extends StatelessWidget {
  @override
  Widget build(BuildContext context) {
    return MaterialApp(
      debugShowCheckedModeBanner: false,
      home: Scaffold(
        appBar: AppBar(
          title: Center(child: Text("You Mangá")),
          backgroundColor: Theme.of(context).secondaryHeaderColor,
        ),
        body: Container(
          decoration: BoxDecoration(
            color: Theme.of(context).backgroundColor,
            image: DecorationImage(
              image: AssetImage('lib/imagens/logo.png'),
            ),
          ),
        ),
        drawer: Drawer(
          child: ListView(
            children: <Widget>[
              // Avatar dentro do menu
              UserAccountsDrawerHeader(
                decoration: BoxDecoration(
                    color: Theme.of(context).secondaryHeaderColor),
                accountName: Text("Janderrson Barbosa"),
                accountEmail: Text("janderson.goncalves@fatec.sp.gov.br"),
                currentAccountPicture: CircleAvatar(
                    radius: 30.0,
                    backgroundImage: AssetImage('lib/imagens/goku2.jpg'),
                    backgroundColor: Theme.of(context).secondaryHeaderColor),
              ),

              // List tile para opção Favoritos
              ListTile(
                  leading: Icon(Icons.star),
                  title: Text("Favoritos"),
                  subtitle: Text("Meus favoritos..."),
                  trailing: Icon(Icons.arrow_forward),
                  onTap: () {
                    Navigator.of(context).pushNamed('/favoritos');
                  }),

              // List tile para opção Perfil
              ListTile(
                  leading: Icon(Icons.book),
                  title: Text("Catálago"),
                  subtitle: Text("Catálogo e Novidades..."),
                  trailing: Icon(Icons.arrow_forward),
                  onTap: () {
                    Navigator.of(context).pushNamed('/catalogo');
                  }),
              ListTile(
                  leading: Icon(Icons.book),
                  title: Text("ListaMangas"),
                  subtitle: Text("Lista pessoal"),
                  trailing: Icon(Icons.arrow_forward),
                  onTap: () {
                    Navigator.of(context).pushNamed('/mangalist');
                  }),
              ListTile(
                  leading: Icon(Icons.account_circle),
                  title: Text("Perfil"),
                  subtitle: Text("Perfil do usuário..."),
                  trailing: Icon(Icons.arrow_forward),
                  onTap: () {
                    Navigator.of(context).pushNamed('/perfil');
                  }),
              ListTile(
                  leading: Icon(Icons.settings),
                  title: Text("Configurações"),
                  subtitle: Text("Configurações do App..."),
                  trailing: Icon(Icons.arrow_forward),
                  onTap: () {
                    Navigator.of(context).pushNamed('/settings');
                  }),
              ListTile(
                  leading: Icon(Icons.flip_to_back_rounded),
                  title: Text("Sair"),
                  subtitle: Text("Sair da Conta"),
                  trailing: Icon(Icons.arrow_forward),
                  onTap: () {
                    Navigator.pop(context);
                  }),
            ],
          ),
        ),
      ),
    );
  }
}
