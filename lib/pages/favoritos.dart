import 'package:flutter/material.dart';

class ListFavoritos extends StatefulWidget {
  @override
  _ListFavoritosState createState() => _ListFavoritosState();
}

class _ListFavoritosState extends State<ListFavoritos> {
  @override
  Widget build(BuildContext context) {
    return Scaffold(
      appBar: AppBar(
        title: Text('Catálogo',
            style: TextStyle(color: Theme.of(context).primaryColor)),
        centerTitle: true,
        backgroundColor: Theme.of(context).secondaryHeaderColor,
        leading: Builder(
          builder: (BuildContext context) {
            return IconButton(
                onPressed: () {
                  Navigator.pop(context);
                },
                icon: Icon(Icons.arrow_back),
                color: Colors.white);
          },
        ),
      ),
      body: Container(
          color: Theme.of(context).backgroundColor,
          child: ListView(
            scrollDirection: Axis.vertical,
            padding: EdgeInsets.all(5),
            children: [
              //Container imagem
              Container(
                height: MediaQuery.of(context).size.height * 1,
                child: Column(
                  mainAxisAlignment: MainAxisAlignment.spaceBetween,
                  children: [
                    Center(
                      child: Stack(
                        alignment: AlignmentDirectional.topCenter,
                        children: <Widget>[
                          Image.asset('lib/imagens/dragonball01.jpg',
                              width: 500, height: 500),
                          Positioned(
                            bottom: 0,
                            child: ElevatedButton.icon(
                              onPressed: () {
                                showAlertDialog1(context);
                              },
                              style: ButtonStyle(
                                  backgroundColor:
                                      MaterialStateProperty.all(Colors.green.shade900)),
                              icon: Icon(Icons.remove),
                              label: Text("Remover"),
                            ),
                          ),
                        ],
                      ),
                    ),
                  ],
                ),
              ),

              //Container imagem
              Container(
                height: MediaQuery.of(context).size.height * 1,
                child: Column(
                  mainAxisAlignment: MainAxisAlignment.spaceBetween,
                  children: [
                    Center(
                      child: Stack(
                        alignment: AlignmentDirectional.topCenter,
                        children: <Widget>[
                          Image.asset('lib/imagens/dragonball02.jpg',
                              width: 500, height: 500),
                          Positioned(
                            bottom: 0,
                            child: ElevatedButton.icon(
                              onPressed: () {
                                showAlertDialog1(context);
                              },
                              style: ButtonStyle(
                                  backgroundColor:
                                      MaterialStateProperty.all(Colors.green.shade900)),
                              icon: Icon(Icons.remove),
                              label: Text("Remover"),
                            ),
                          ),
                        ],
                      ),
                    ),
                  ],
                ),
              ),
              Container(
                height: MediaQuery.of(context).size.height * 1,
                child: Column(
                  mainAxisAlignment: MainAxisAlignment.spaceBetween,
                  children: [
                    Center(
                      child: Stack(
                        alignment: AlignmentDirectional.topCenter,
                        children: <Widget>[
                          Image.asset('lib/imagens/dragonball03.jpg',
                              width: 500, height: 500),
                          Positioned(
                            bottom: 0,
                            child: ElevatedButton.icon(
                              onPressed: () {
                                showAlertDialog1(context);
                              },
                              style: ButtonStyle(
                                  backgroundColor:
                                      MaterialStateProperty.all(Colors.green.shade900)),
                              icon: Icon(Icons.remove),
                              label: Text("Remover"),
                            ),
                          ),
                        ],
                      ),
                    ),
                  ],
                ),
              ),
            ],
          )),
    );
  }
}

showAlertDialog1(BuildContext context) {
  // configura o button
  Widget okButton = TextButton(
    child: Text("OK"),
    onPressed: () {
      Navigator.of(context).pop();
    },
  );
  // configura o  AlertDialog
  AlertDialog alerta = AlertDialog(
    title: Text(
      "Aviso!",
      style: TextStyle(color: Colors.red),
    ),
    content: Text("Mangá removido dos Favoritos com sucesso"),
    actions: [
      okButton,
    ],
  );
  // exibe o dialog
  showDialog(
    context: context,
    builder: (BuildContext context) {
      return alerta;
    },
  );
}
